from django.urls import path

from . import  views

# path() function
# path(route, view, name)
urlpatterns = [
	path('', views.index, name='index'),
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),

	# ./register
	path('register', views.register, name="register"),

	# ./change_password
	path('change_password', views.change_password, name="change_password"),

	# ./login
	path('login', views.login_view, name="login"),

	# ./logout
	path('logout', views.logout_view, name="logout")
]